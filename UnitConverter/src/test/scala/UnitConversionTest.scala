import org.scalatest.FunSuite

class UnitConversionTest extends FunSuite{

  test("Conversion MM to Inch"){
    val fromValue = 5
    val result = MMtoInches.run(fromValue toString)
    assert(result.equals("0.1968503937007874"))
  }

  test("Conversion Inch to MM"){
    val fromValue = 10
    val result = InchesToMM.run(fromValue toString)
    assert(result.equals("254.0"))
  }
}
