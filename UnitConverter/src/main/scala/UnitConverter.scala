import Bundle.resourceBundle
import LocalizationLabels.{inchToMMLabel, mmToInchLabel}

trait UnitConverter {
  val description: String
  def run(input: String): String

  override def toString = description
}

class UnitConverters(converters: UnitConverter*) {
  val available = List(converters : _*)
}

object MMtoInches extends UnitConverter {
  val description: String = resourceBundle.getString(mmToInchLabel)
  def run(input: String): String = try { (input.toDouble / 25.4).toString }
                                    catch { case _: Throwable => "Exception" }
}

object InchesToMM extends UnitConverter {
  val description: String = resourceBundle.getString(inchToMMLabel)
  def run(input: String): String = try { (input.toDouble * 25.4).toString }
                                    catch { case _: Throwable => "Exception" }
}