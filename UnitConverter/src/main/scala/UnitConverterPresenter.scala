import DialogUtils._
import LocalizationLabels._
import javafx.beans.binding.StringBinding
import javafx.scene.control.Label
import scalafx.application.Platform
import scalafx.event.ActionEvent
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._
import scalafx.stage.Stage
import scalafxml.core.macros.sfxml
import Bundle._

@sfxml
class UnitConverterPresenter(from: TextField,
                             to: TextField,
                             types: ComboBox[UnitConverter],
                             closeButton: Button,
                             toLabel: Label,
                             fromLabel: Label,
                             convTypeLabel: Label,
                             converters: UnitConverters) {

  // Setting components text
  closeButton.text = resourceBundle.getString(closeButtonLabel)
  toLabel.setText(resourceBundle.getString(LocalizationLabels.toLabel))
  fromLabel.setText(resourceBundle.getString(LocalizationLabels.fromLabel))
  convTypeLabel.setText(resourceBundle.getString(LocalizationLabels.convTypeLabel))

  // Filling the combo box
  converters.available foreach(converter => types += converter)
  types.getSelectionModel.selectFirst()

  // Data binding
  to.text <== new StringBinding {
    bind(from.text.delegate, types.getSelectionModel.selectedItemProperty)

    def computeValue() = {
      val result = types.getSelectionModel.getSelectedItem.run(from.text.value)
      result match {
        case "Exception" => resourceBundle.getString(exceptionLabel)
        case _ => result
      }
    }
  }

  // Close button event handler
  def onClose(event: ActionEvent) {
    def closeHandler(result: Option[ButtonType]) = result match {
      case Some(ButtonType.OK) => Platform.exit()
      case _                   => println("Dialog closed")
    }
    closeHandler(infoDialog(Application.stage, resourceBundle.getString(closeDialogTitle),
      resourceBundle.getString(closeDialogHeader), resourceBundle.getString(closeDialogContent)))
  }
}

object DialogUtils {
  def infoDialog(stage: Stage, alertTitle: String, alertHeaderText: String,
                 alertContentText: String): Option[ButtonType] = {
    new Alert(AlertType.Confirmation) {
      initOwner(stage)
      title = alertTitle
      headerText = alertHeaderText
      contentText = alertContentText
    }.showAndWait()
  }
}