import java.util.{Locale, ResourceBundle}

import Bundle.resourceBundle

import scala.reflect.runtime.universe.typeOf
import scalafx.application.JFXApp
import scalafx.Includes._
import scalafx.scene.Scene
import scalafx.scene.image.Image
import scalafxml.core.{DependenciesByType, FXMLView}


object Bundle{
  val resourceBundle: ResourceBundle = ResourceBundle.getBundle("Localization",
    new Locale("it", "IT"))
}

/** A simple ScalaFXML Application that provides conversion utilities
  *
  * @author Nicola Piscaglia
  *
  * */
object Application extends JFXApp {
  private val appLogoPath = "app_logo.png"

  val root = FXMLView(getClass.getResource("unitconverter.fxml"),
    new DependenciesByType(Map(
      typeOf[UnitConverters] -> new UnitConverters(InchesToMM, MMtoInches))))

  stage = new JFXApp.PrimaryStage() {
    title = resourceBundle.getString(LocalizationLabels.appTitleLabel)
    scene = new Scene(root)
  }

  stage getIcons() add new Image(appLogoPath)
}


object LocalizationLabels{
  val convTypeLabel= "CONV_TYPE_LABEL_TEXT"
  val fromLabel = "FROM_LABEL_TEXT"
  val toLabel = "TO_LABEL_TEXT"
  val mmToInchLabel = "MILLIMETERS_TO_INCHES_TEXT"
  val inchToMMLabel = "INCHES_TO_MILLIMETERS_TEXT"
  val exceptionLabel = "EXCEPTION_TEXT"
  val appTitleLabel = "APP_TITLE"
  val closeButtonLabel = "CLOSE_BUTTON_TEXT"
  val selectLanguageLabel = "SEL_LANG_TEXT"
  val closeDialogTitle = "CLOSE_DIALOG_TITLE"
  val closeDialogHeader = "CLOSE_DIALOG_HEADER"
  val closeDialogContent = "CLOSE_DIALOG_CONTENT"
}
