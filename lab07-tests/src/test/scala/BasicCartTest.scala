import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.MockFactory
import org.scalatest.FunSuite
import u07lab._

class BasicCartTest extends FunSuite with MockFactory with Matchers{

  def fixture = new {
    val warehouse = new BasicWarehouse()
    val cart = mock[Cart]
    val fun = stubFunction[Cart, String]
    fun.when(*).returns(cart.toString)
  }

  test("An empty cart should have no items"){
    assert(new BasicCart().size == 0)
  }

  test("Adding an item produce a non-empty cart"){
    val cart: BasicCart = new BasicCart()
    cart.add(Item(Product("giocattolo"),
      ItemDetails(1, Price(50.0))))
    assert(cart.size == 1)
  }

  test("Adding an item produce a non-empty cart with the correct item"){
    val cart: BasicCart = new BasicCart()
    val item: Item = Item(Product("giocattolo"),
      ItemDetails(1, Price(50.0)))
    cart.add(item)
    assert(cart.content.contains(item))
  }

  test("Adding more elements produce a correct total cost"){
    val cart: BasicCart = new BasicCart()
    val item: Item = Item(Product("giocattolo"),
      ItemDetails(1, Price(50.0)))
    val item2: Item = Item(Product("vino"),
      ItemDetails(1, Price(50.0)))
    cart.add(item)
    cart.add(item2)
    assert(cart.totalCost == 100)
  }

  test("products are correctly inserted in the cart"){
    val f = fixture

    val product1 = Product("Wine")
    val product2 = Product("Sugar")
    val item = Item(product1, ItemDetails(1, Price(50)))
    val item2 = Item(product2, ItemDetails(1, Price(60)))

    //Arrange: supply the warehouse
    f.warehouse.supply(product1, 3)
    f.warehouse.supply(product2, 5)


    //Add items to cart
    if (f.warehouse.get(product1,3) == (product1,3) &&
        f.warehouse.get(product2, 3) == (product2, 3)){

      inSequence{
        (f.cart.add _).expects(item)
        (f.cart.add _).expects(item2)
        (f.cart.content _).expects().returning(Set(item, item2))
        (f.cart.totalCost _).expects().returning(110)
      }

      f.cart.add(item)
      f.cart.add(item2)
      val cardContent = f.cart.content
      f.fun(f.cart)
      println("Content of the card: " + cardContent)
      println("Total cost: " + f.cart.totalCost)
    }

    f.fun.verify(f.cart)
  }

}
