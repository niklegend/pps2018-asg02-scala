import org.scalatest.FlatSpec
import u07lab._

class BasicCatalogTest extends FlatSpec{

  it should "return the right price for each product" in{
    val item = Product("giocattolo")
    val item2 = Product("vino")
    var map:Map[Product, Price] = Map()
    map = map + (item -> Price(50.0))
    map = map + (item2 -> Price(75.0))
    val catalog:BasicCatalog = new BasicCatalog(map)
    assert(catalog.products == map)
    println(map)
    assert(catalog.priceFor(item, 1) == Price(50.0) &&
      catalog.priceFor(item2, 1) == Price(75.0))
  }
}
