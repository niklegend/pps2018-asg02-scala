val scalatest = "org.scalatest" % "scalatest_2.12" % "3.0.1" % "test"
lazy val root = (project in file (".")).settings(
  name := "pps-lab-testing-piscaglia", version := "1.0",
  organization := "unibo.pps", scalaVersion := "2.12.2",
  libraryDependencies ++= Seq(scalatest),
  libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test)
