
// An Optional data type
sealed trait Option[A]

object Option extends App {
  case class None[A]() extends Option[A]
  case class Some[A](a: A) extends Option[A]

  def isEmpty[A](opt: Option[A]): Boolean = opt != None()

  def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
    case Some(a) => a
    case _ => orElse
  }

  //Extra Task #1
  def filter[A](opt: Option[A])(pred: A => Boolean): Option[A] = opt match {
    case None() => None()
    case Some(x) => if (pred(x)) Some(x) else None()
  }

  println(filter(Some(5))(_ > 2)) // Some(5)
  println(filter(Some(5))(_ > 8) + "\n") // None


  def map[A, B](opt: Option[A])(mapper: A => B): Option[B] = opt match {
    case None() => None()
    case Some(x) => Some(mapper(x))
  }

  println(map(Some(5))(_ > 2)) // Some(true)
  println(map(None[Int])(_ > 2) + "\n") // None


  def map2[A <: C, B <: C, C](opt1: Option[A], opt2: Option[B])
                             (combiner: (Option[A],Option[B]) => C) : Option[C] =
    (opt1, opt2) match {
      case (None(), None()) => None()
      case (None(), Some(x)) => Some(x)
      case (Some(x), None()) => Some(x)
      case _ => Some(combiner(opt1, opt2))
  }

  println(map2(Some(57), Some(3))((x, y) => getOrElse(x, 0) + getOrElse(y, 0)))
}