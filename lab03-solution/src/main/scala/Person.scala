import List.{Cons, Nil, filter, map}
// Sum type: a sealed base trait, and cases extending it

sealed trait Person//sealed: no other impl. except Student, Teacher

  case class Student(name: String, year: Int) extends Person

  case class Teacher(name: String, course: String) extends Person


object UseTeacher extends App{

  def name(p: Person): String = p match {
    case Student(n, _) => n
    case Teacher(n, _) => n
  }

  def course(t: Person): String = t match {
    case Teacher(_, c) => c
    case _ => ""
  }

  // Advanced Esercise
  def getTeacherCourses(teacherList: List[Person]): List[String] = teacherList match {
    case Nil() => Nil()
    case _ => map(filter(teacherList)(p => p.isInstanceOf[Teacher]))(t => course(t))
  }

  val teacherList = Cons[Person](Teacher("mario", "LCMC"), Cons(Teacher("giuseppe", "PPS"),
    Cons(Student("marco", 1995), Nil())))

  println(getTeacherCourses(teacherList))
}



