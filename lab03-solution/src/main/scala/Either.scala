sealed trait Either[L,R]

object Either extends App {

  case class Error[L,R](err: L) extends Either[L,R]

  case class Success[L,R](succ: R) extends Either[L,R]


}
