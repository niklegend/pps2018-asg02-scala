import scala.annotation.tailrec

object Fibonacci extends App {

  // Head Recursion Version
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case _ => fib(n - 1) + fib(n - 2)
  }

  println((fib(0), fib(1), fib(2), fib(40)))

  /**
    * Calcolo del numero di Fibonacci con una ricorsione Tail
    *
    * @param n la posizione del numero di fibonacci da calcolare
    * @return il numero di fibonacci calcolato
    */
  def fibTail(n: Int): Int = {

    @tailrec
    def _fib(accumulator: => Int, prevAccumulator: => Int, actualFibPos: => Int): Int =
      actualFibPos match {
        case actual if actual == n => accumulator
        case _ => _fib(accumulator + prevAccumulator, accumulator, actualFibPos + 1)
      }

    n match {
      case 0 | 1 => n
      case _ => _fib(1, 1, 2)
    }
  }

  println((fibTail(0), fibTail(1), fibTail(2), fibTail(40)))
}
