import Option.None
import Option.Some

// A generic linkedlist
sealed trait List[E]

// a companion object (i.e., module) for List
object List extends App {
  case class Cons[E](head: E, tail: List[E]) extends List[E]
  case class Nil[E]() extends List[E]

  def length[E](l: List[E]): Int = l match {
    case Cons(h, t) => 1 + length(t)
    case _ => 0
  }

  def sum(l: List[Int]): Int = l match {
    case Cons(h, t) => h + sum(t)
    case _ => 0
  }

  def append[A <: C, B <: C, C](l1: List[A], l2: List[B]): List[C] = (l1, l2) match {
    case (Cons(h, t), l2) => Cons[C](h, append(t, l2))
    case (l1, Cons(h, t)) => Cons[C](h, append(l1, t))
    case _ => Nil()
  }

  // Drop Exercise
  def drop[A](l: List[A], n: Int): List[A] = (l,n) match {
    case (Nil(), _) => Nil()
    case (_, 0) => l
    case (Cons(_, tail), _) => drop(tail, n - 1)
  }

  println(Cons(10, Cons(20, Cons(30, Nil()))))
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),2))
  println(drop(Cons(10, Cons(20, Cons(30, Nil()))),5) + "\n")


  //Map Exercise
  def map[A,B](l: List[A])(f: A => B): List[B] = (l,f) match {
    case (Nil(), _) => Nil()
    case (Cons(h, t), _) => Cons(f(h), map(t)(f))
  }

  println(map(Cons(10, Cons(20, Nil())))(_+1)) // Cons(11, Cons(21, Nil()))
  println(map(Cons(10, Cons(20, Nil())))(":"+_+":") + "\n") // Cons(":10:", Cons(":20:", Nil()))


  //Filter Exercise
  def filter[A](l: List[A])(f: A => Boolean): List[A] = {
    var list: List[A] = Nil()
    def filter_(l: List[A], acc: List[A])(f: A => Boolean): Unit = l match {
      case Nil() => None()
      case Cons(h, t) =>
        if (f(h)) list = append(list, Cons(h,Nil()))
        filter_(t, list)(f)
    }
    filter_(l, Nil())(f)
    list
  }

  println(filter(Cons(10, Cons(20, Nil())))(_>15)) // Cons(20, Nil())
  println(filter(Cons("a", Cons("bb", Cons("ccc", Nil()))))( _.length <=2) + "\n") // Cons("a", Cons("bb", Nil()))


  //Max Exercise
  def max(l: List[Int]): Option[Int] = {

      def max_(l: List[Int], max: Int): Option[Int] = l match {
        case Nil() => Some(max)
        case Cons(h, tail) => if (h > max) max_(l, h) else max_(tail, max)
      }

        l match {
          case Nil() => None()
          case Cons(h, Nil()) => Some(h)
          case _ => max_(l, 0)
        }
  }

  println(max(Cons(10, Cons(25, Cons(20, Nil()))))) // Some(25)
  println(max(Nil()) + "\n") // None()


  // Extra Task #2
  def foldRight[A](lst: List[A])(default: A)(accumulator: (A,A) => A): A = lst match {
    case Nil() => default
    case Cons(h, tail) => foldRight(tail)(accumulator(h, default))(accumulator)
  }

  def foldLeft[A](lst: List[A])(default: A)(accumulator: (A,A) => A): A = {

    def reverse[A](l: List[A]): List[A] = l match {
      case Cons(h, tail) => append(reverse(tail), Cons(h, Nil()))
      case Nil() => Nil()
    }

    lst match {
      case Nil() => default
      case Cons(_, _) => foldRight(reverse(lst))(default)(accumulator)
    }
  }

  val lst = Cons("3",Cons("7",Cons("1",Cons("5", Nil()))))
  println(foldLeft(lst)("")(_+_)) // "3715"
  println(foldRight(lst)("")(_+_)) // "5173"
}
